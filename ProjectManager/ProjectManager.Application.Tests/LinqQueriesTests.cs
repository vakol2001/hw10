using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.Infrastructure.Mapper.MappingProfiles;
using ProjectManager.Application.LinqQueries.Queries;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using ProjectManager.Data.Enums;
using System;
using System.Linq;
using Xunit;
using Task = System.Threading.Tasks.Task;
using TaskProject = ProjectManager.Data.Entities.Task;

namespace ProjectManager.Application.Tests
{
    public class LinqQueriesTests : IDisposable
    {
        readonly ProjectManagerContext _context;
        readonly IMapper _mapper;
        public LinqQueriesTests()
        {
            _mapper = new MapperConfiguration(conf =>
            {
                conf.AddProfile<ProjectMappingProfile>();
                conf.AddProfile<TaskMappingProfile>();
                conf.AddProfile<TeamMappingProfile>();
                conf.AddProfile<UserMappingProfile>();
            }).CreateMapper();

            var options = new DbContextOptionsBuilder<ProjectManagerContext>().UseInMemoryDatabase("TestLinqDatabase").Options;
            _context = new ProjectManagerContext(options);
            _context.Database.EnsureCreated();
            var teams = Enumerable.Range(1, 2)
                .Select(x => new Team
                {
                    Id = x,
                    Name = $"Team{x}",
                }).ToList();

            teams[0].Participants = Enumerable.Range(1, 5)
                .Select(x => new User
                {
                    Id = x,
                    BirthDay = new DateTime(1990 - x, 11, 11),
                    FirstName = $"User{x}",
                }).ToList();
            teams[1].Participants = Enumerable.Range(6, 5)
                .Select(x => new User
                {
                    Id = x,
                    BirthDay = new DateTime(2010 + x, 11, 11),
                    FirstName = $"User{x}",
                }).ToList();

            teams[0].Participants.ElementAt(4).CreatedAt = new DateTime(1990, 10, 10);

            var projects = Enumerable.Range(1, 4)
                .Select(x => new Project
                {
                    Id = x,
                    Name = $"Project{x}",
                    AuthorId = x
                }).ToList();


            projects[0].TeamId = 1;
            projects[1].TeamId = 1;
            projects[2].TeamId = 2;

            projects[3].AuthorId = 1;

            projects[0].Description = "more than 20 length string";

            projects[0].Tasks = Enumerable.Range(1, 5)
                .Select(x => new TaskProject
                {
                    Id = x,
                    Name = $"Task{x}",
                    State = TaskState.Done,
                }).ToList();
            projects[1].Tasks = Enumerable.Range(6, 4)
                .Select(x => new TaskProject
                {
                    Id = x,
                    Name = $"Task{x}",
                    State = TaskState.Done,
                }).ToList();
            projects[2].Tasks = Enumerable.Range(10, 5)
                .Select(x => new TaskProject
                {
                    Id = x,
                    Name = $"Task{x}",
                    State = TaskState.Done,
                }).ToList();
            projects[0].Tasks.ElementAt(4).Description = "Long Description";

            projects[0].Tasks.ElementAt(0).PerformerId = 2;
            projects[0].Tasks.ElementAt(1).PerformerId = 2;
            projects[0].Tasks.ElementAt(2).PerformerId = 2;

            projects[0].Tasks.ElementAt(2).Name = "Task with name, which is longer than 45 symbols";

            projects[0].Tasks.ElementAt(1).FinishedAt = new DateTime(DateTime.Now.Year, 1, 1);

            _context.Teams.AddRange(teams);
            _context.Projects.AddRange(projects);

            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Theory]
        [InlineData(1, 2)]
        [InlineData(2, 1)]
        [InlineData(3, 1)]
        public async Task GetProjectInfoByUserIdQuery_WhenNProjectsForUser_ThenNProjects(int id, int count)
        {
            var handler = new GetProjectInfoByUserIdQuery.Handler(_context);

            var result = await handler.Handle(new GetProjectInfoByUserIdQuery { Id = id }, default);

            Assert.Equal(count, result.Count);

        }
        [Fact]
        public async Task GetProjectInfoByUserIdQuery_WhenNoProjects_ThenZeroProjects()
        {
            var handler = new GetProjectInfoByUserIdQuery.Handler(_context);

            var result = await handler.Handle(new GetProjectInfoByUserIdQuery { Id = 7 }, default);

            Assert.Empty(result);

        }

        [Fact]
        public async Task GetProjectsInfoQuery_When2MatchingProjects_Then2Projects()
        {
            var handler = new GetProjectsInfoQuery.Handler(_mapper, _context);

            var result = await handler.Handle(new GetProjectsInfoQuery(), default);

            Assert.Equal(2, result.Count());

        }

        [Fact]
        public async Task GetProjectsInfoQuery_WhenNoTaskInProject_ThenNullTasks()
        {
            var handler = new GetProjectsInfoQuery.Handler(_mapper, _context);

            var result = await handler.Handle(new GetProjectsInfoQuery(), default);

            Assert.Null(result.First(x => x.Project.Id == 4).LongestTaskByDescription);
            Assert.Null(result.First(x => x.Project.Id == 4).ShortestTaskByName);

        }

        [Fact]
        public async Task GetProjectsInfoQuery_WhenLongestTaskWithId5_ThenLongestTaskWithId5()
        {
            var handler = new GetProjectsInfoQuery.Handler(_mapper, _context);

            var result = await handler.Handle(new GetProjectsInfoQuery(), default);

            Assert.Equal(5, result.First(x => x.Project.Id == 1).LongestTaskByDescription.Id);

        }

        [Fact]
        public async Task GetSortedUsersWithSortedTasksQuery_When10Users_ThenOrdered10Users()
        {
            var handler = new GetSortedUsersWithSortedTasksQuery.Handler(_mapper, _context);

            var result = (await handler.Handle(new GetSortedUsersWithSortedTasksQuery(), default)).ToList();

            Assert.Equal("User1", result[0].FirstName);
            Assert.Equal("User10", result[1].FirstName);
            Assert.Equal("User2", result[2].FirstName);
            Assert.Equal("User3", result[3].FirstName);
            Assert.Equal("User4", result[4].FirstName);
            Assert.Equal("User5", result[5].FirstName);
            Assert.Equal("User6", result[6].FirstName);
            Assert.Equal("User7", result[7].FirstName);
            Assert.Equal("User8", result[8].FirstName);
            Assert.Equal("User9", result[9].FirstName);

        }

        [Fact]
        public async Task GetTasksWichFinishedInCurrentYearByUserIdQuery_WhenNoTasks_ThenEmpty()
        {
            var handler = new GetTasksWichFinishedInCurrentYearByUserIdQuery.Handler(_mapper, _context);

            var result = await handler.Handle(new GetTasksWichFinishedInCurrentYearByUserIdQuery { Id = 1 }, default);

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetTasksWichFinishedInCurrentYearByUserIdQuery_When1Task_ThenSingle()
        {
            var handler = new GetTasksWichFinishedInCurrentYearByUserIdQuery.Handler(_mapper, _context);

            var result = await handler.Handle(new GetTasksWichFinishedInCurrentYearByUserIdQuery { Id = 2 }, default);

            Assert.Single(result);
        }

        [Fact]
        public async Task GetTasksWithShortNameByUserIdQuery_WhenNoTasks_ThenEmpty()
        {
            var handler = new GetTasksWithShortNameByUserIdQuery.Handler(_mapper, _context);

            var result = await handler.Handle(new GetTasksWithShortNameByUserIdQuery { Id = 1 }, default);

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetTasksWithShortNameByUserIdQuery_When2MathingTasks_Then2Tasks()
        {
            var handler = new GetTasksWithShortNameByUserIdQuery.Handler(_mapper, _context);

            var result = await handler.Handle(new GetTasksWithShortNameByUserIdQuery { Id = 2 }, default);

            Assert.Equal(2, result.Count());
        }

        [Fact]
        public async Task GetTeamsWithParticipantsOlderThan10Query_When1MatchingTeam_ThenSingle()
        {
            var handler = new GetTeamsWithParticipantsOlderThan10Query.Handler(_mapper, _context);

            var result = await handler.Handle(new GetTeamsWithParticipantsOlderThan10Query(), default);

            Assert.Single(result);

            Assert.Equal(5, result.First().Participants.Last().Id);
        }

        [Fact]
        public async Task GetUserInfoByIdQuery_WhenUserId1_ThenUserId1()
        {
            var handler = new GetUserInfoByIdQuery.Handler(_mapper, _context);

            var result = await handler.Handle(new GetUserInfoByIdQuery() { Id = 1 }, default);

            Assert.Equal(1, result.User.Id);
        }

        [Fact]
        public async Task GetUserInfoByIdQuery_When2NotFinishedTask_Then2Tasks()
        {
            var handler = new GetUserInfoByIdQuery.Handler(_mapper, _context);

            var result = await handler.Handle(new GetUserInfoByIdQuery() { Id = 2 }, default);

            Assert.Equal(2, result.AmountNotFinishedTasks);
        }
    }
}
