﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.Infrastructure.Mapper.MappingProfiles;
using ProjectManager.Application.Tasks.Queries;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System;
using System.Linq;
using Xunit;
using Task = System.Threading.Tasks.Task;
using TaskProject = ProjectManager.Data.Entities.Task;

namespace ProjectManager.Application.Tests
{
    public class UnfinishedTaskTests : IDisposable
    {
        readonly ProjectManagerContext _context;
        readonly IMapper _mapper;
        public UnfinishedTaskTests()
        {
            _mapper = new MapperConfiguration(conf =>
            {
                conf.AddProfile<ProjectMappingProfile>();
                conf.AddProfile<TaskMappingProfile>();
                conf.AddProfile<TeamMappingProfile>();
                conf.AddProfile<UserMappingProfile>();
            }).CreateMapper();

            var options = new DbContextOptionsBuilder<ProjectManagerContext>().UseInMemoryDatabase("TestUnfinishedTaskDatabase").Options;
            _context = new ProjectManagerContext(options);
            _context.Database.EnsureCreated();

            _context.Users.Add(new User
            {
                Id = 1,
                Tasks = Enumerable.Range(1, 5)
                    .Select(x => new TaskProject { Id = x, Name = "Task" + x })
                    .Append(new TaskProject { Id = 6, Name = "Task6", FinishedAt = DateTime.Now })
                    .ToList()
            });

            _context.Users.Add(new User
            {
                Id = 3,
                Tasks = Enumerable.Range(7, 5)
                    .Select(x => new TaskProject { Id = x, Name = "Task" + x, FinishedAt = DateTime.Now })
                    .ToList()
            });
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Fact]
        public async Task GetUnfinishedTasksByUserIdQuery_When5Tasks_ThenAllTasksUnfinished()
        {

            var handler = new GetUnfinishedTasksByUserIdQuery.Handler(_mapper, _context);

            var request = new GetUnfinishedTasksByUserIdQuery { UserId = 1 };

            var response = await handler.Handle(request, default);

            foreach (var task in response)
            {
                Assert.Null(task.FinishedAt);
            }
        }

        [Fact]
        public async Task GetUnfinishedTasksByUserIdQuery_When5Tasks_Then5Tasks()
        {

            var handler = new GetUnfinishedTasksByUserIdQuery.Handler(_mapper, _context);

            var request = new GetUnfinishedTasksByUserIdQuery { UserId = 1 };

            var response = await handler.Handle(request, default);

            Assert.Equal(5, response.Count());
        }

        [Fact]
        public async Task GetUnfinishedTasksByUserIdQuery_WhenNoUser_ThenEmpty()
        {

            var handler = new GetUnfinishedTasksByUserIdQuery.Handler(_mapper, _context);

            var request = new GetUnfinishedTasksByUserIdQuery { UserId = 2 };

            var response = await handler.Handle(request, default);

            Assert.Empty(response);
        }

        [Fact]
        public async Task GetUnfinishedTasksByUserIdQuery_WhenAllFinished_ThenEmpty()
        {

            var handler = new GetUnfinishedTasksByUserIdQuery.Handler(_mapper, _context);

            var request = new GetUnfinishedTasksByUserIdQuery { UserId = 3 };

            var response = await handler.Handle(request, default);

            Assert.Empty(response);
        }
    }
}
