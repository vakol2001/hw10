﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.Infrastructure.Mapper.MappingProfiles;
using ProjectManager.Application.Tasks.Commands;
using ProjectManager.Data;
using ProjectManager.Data.Enums;
using System;
using System.Collections.Generic;
using Xunit;
using Task = System.Threading.Tasks.Task;
using TaskProject = ProjectManager.Data.Entities.Task;

namespace ProjectManager.Application.Tests
{
    public class TaskTests : IDisposable
    {
        readonly ProjectManagerContext _context;
        readonly IMapper _mapper;
        public TaskTests()
        {
            _mapper = new MapperConfiguration(conf =>
            {
                conf.AddProfile<ProjectMappingProfile>();
                conf.AddProfile<TaskMappingProfile>();
                conf.AddProfile<TeamMappingProfile>();
                conf.AddProfile<UserMappingProfile>();
            }).CreateMapper();

            var options = new DbContextOptionsBuilder<ProjectManagerContext>().UseInMemoryDatabase("TestTaskDatabase").Options;
            _context = new ProjectManagerContext(options);
            _context.Database.EnsureCreated();
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Fact]
        public async Task UpdateTaskCommand_WhenTaskNotFound_ThenThrowsKeyNotFoundExeption()
        {
            var handler = new UpdateTaskCommand.Handler(_mapper, _context);

            var request = new UpdateTaskCommand
            {
                Id = 999,
                FinishedAt = DateTime.Now
            };

            await Assert.ThrowsAsync<KeyNotFoundException>(async () => await handler.Handle(request, default));
        }

        [Fact]
        public async Task UpdateTaskCommand_WhenUpdateTaskToFinished_ThenFinishedAtPropertyNotNull()
        {
            _context.Tasks.Add(new TaskProject { Id = 1, Name = "Tast", State = TaskState.ToDo });
            _context.SaveChanges();

            var handler = new UpdateTaskCommand.Handler(_mapper, _context);

            var request = new UpdateTaskCommand
            {
                Id = 1,
                FinishedAt = DateTime.Now
            };

            await handler.Handle(request, default);

            Assert.NotNull(_context.Tasks.Find(1).FinishedAt);
        }

    }


}
