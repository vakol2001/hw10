﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProjectManager.Application.Projects.Commands;
using ProjectManager.Application.Projects.Models;
using ProjectManager.Application.Projects.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManager.WebAPI.Controllers.v1
{
    public class ProjectsController : ApiController
    {
        public ProjectsController(IMediator mediator) : base(mediator) { }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectModel>>> GetAll()
        {
            return Ok(await Mediator.Send(new GetAllTasksQuery()));
        }
        [HttpGet("{Id}")]
        public async Task<ActionResult<ProjectModel>> GetById([FromRoute] GetProjectByIdQuery query)
        {
            try
            {
                return Ok(await Mediator.Send(query));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

        }
        [HttpPost]
        public async Task<ActionResult<ProjectModel>> Post([FromBody] UpdateProjectCommand command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }
        [HttpPut]
        public async Task<ActionResult<ProjectModel>> Put([FromBody] CreateProjectCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpDelete("{Id}")]
        public async Task<ActionResult> Delete([FromRoute] DeleteProjectCommand command)
        {
            try
            {
                await Mediator.Send(command);
                return NoContent();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

        }
    }
}
