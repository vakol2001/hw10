﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Tasks.Queries
{
    public class GetUnfinishedTasksByUserIdQuery : IRequest<IEnumerable<TaskModel>>
    {
        public int UserId { get; set; }

        public class Handler : IRequestHandler<GetUnfinishedTasksByUserIdQuery, IEnumerable<TaskModel>>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<IEnumerable<TaskModel>> Handle(GetUnfinishedTasksByUserIdQuery request, CancellationToken cancellationToken)
            {
                var tasks = await _context.Tasks
                    .Where(t => t.PerformerId == request.UserId && !t.FinishedAt.HasValue)
                    .ToListAsync(cancellationToken);
                var mappedTasks = _mapper.Map<IEnumerable<TaskModel>>(tasks);
                return mappedTasks;
            }
        }
    }
}
