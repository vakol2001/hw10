﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.Users.Models;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Users.Queries
{
    public class GetAllUsersQuery : IRequest<IEnumerable<UserModel>>
    {
        public class Handler : IRequestHandler<GetAllUsersQuery, IEnumerable<UserModel>>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<IEnumerable<UserModel>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
            {
                var users = await _context.Users.ToListAsync(cancellationToken);
                var mappedUsers = _mapper.Map<IEnumerable<UserModel>>(users);
                return mappedUsers;
            }
        }
    }
}
