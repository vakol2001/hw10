﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.LinqQueries.Models;
using ProjectManager.Application.Projects.Models;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.LinqQueries.Queries
{
    public class GetProjectsInfoQuery : IRequest<IEnumerable<ProjectInfoModel>>
    {
        public class Handler : IRequestHandler<GetProjectsInfoQuery, IEnumerable<ProjectInfoModel>>
        {

            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<IEnumerable<ProjectInfoModel>> Handle(GetProjectsInfoQuery request, CancellationToken cancellationToken)
            {
                return (await _context.Projects.Where(p => p.Description.Length > 20 || p.Tasks.Count < 3)
                .Select(p => new
                {
                    Project = p,
                    LongestTaskByDescription = p.Tasks.OrderBy(t => t.Description.Length).LastOrDefault(),
                    ShortestTaskByName = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                    Users = _context.Users.Where(u => u.TeamId == p.TeamId).ToList()
                }).ToListAsync(cancellationToken))
                .Select(x => new ProjectInfoModel {
                    Project = _mapper.Map<ProjectModel>(x.Project),
                    LongestTaskByDescription = _mapper.Map<TaskModel>(x.LongestTaskByDescription),
                    ShortestTaskByName = _mapper.Map<TaskModel>(x.ShortestTaskByName),
                    ParticipantsAmount = x.Users.Count
                });
            }
        }
    }
}
