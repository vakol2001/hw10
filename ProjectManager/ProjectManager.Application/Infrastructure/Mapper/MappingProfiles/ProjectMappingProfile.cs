﻿using AutoMapper;
using ProjectManager.Application.Projects.Commands;
using ProjectManager.Application.Projects.Models;
using ProjectManager.Data.Entities;

namespace ProjectManager.Application.Infrastructure.Mapper.MappingProfiles
{
    public class ProjectMappingProfile : Profile

    {
        public ProjectMappingProfile()
        {
            CreateMap<Project, ProjectModel>();
            CreateMap<CreateProjectCommand, Project>();
            CreateMap<UpdateProjectCommand, Project>();
        }
    }
}
