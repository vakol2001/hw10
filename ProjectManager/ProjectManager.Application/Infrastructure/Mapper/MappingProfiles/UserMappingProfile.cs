﻿using AutoMapper;
using ProjectManager.Application.LinqQueries.Models;
using ProjectManager.Application.Users.Commands;
using ProjectManager.Application.Users.Models;
using ProjectManager.Data.Entities;

namespace ProjectManager.Application.Infrastructure.Mapper.MappingProfiles
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<User, UserModel>().ForMember("RegisteredAt", opt => opt.MapFrom(u => u.CreatedAt));
            CreateMap<User, UserTasksModel>();
            CreateMap<CreateUserCommand, User>();
            CreateMap<UpdateUserCommand, User>();
        }
    }
}
