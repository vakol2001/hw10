﻿using ProjectManager.BL.Entities;
using ProjectManager.BL.Models;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ProjectManager.BL.Interfaces
{
    public interface IConnectService : IDisposable
    {
        System.Threading.Tasks.Task<IEnumerable<Project>> GetProjectsAsync(CancellationToken cancellationToken = default);
        System.Threading.Tasks.Task<Project> GetProjectAsync(int id, CancellationToken cancellationToken = default);
        System.Threading.Tasks.Task<IEnumerable<Task>> GetTasksAsync(CancellationToken cancellationToken = default);
        System.Threading.Tasks.Task<Task> GetTaskAsync(int id, CancellationToken cancellationToken = default);
        System.Threading.Tasks.Task<IEnumerable<User>> GetUsersAsync(CancellationToken cancellationToken = default);
        System.Threading.Tasks.Task<User> GetUserAsync(int id, CancellationToken cancellationToken = default);
        System.Threading.Tasks.Task<IEnumerable<Team>> GetTeamsAsync(CancellationToken cancellationToken = default);
        System.Threading.Tasks.Task<Team> GetTeamAsync(int id, CancellationToken cancellationToken = default);

        System.Threading.Tasks.Task PostTaskAsync(UpdateTaskModel model, CancellationToken cancellationToken = default);
    }
}
