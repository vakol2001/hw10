﻿using Microsoft.EntityFrameworkCore;
using ProjectManager.Data.Entities;
using System.Linq;
using System;
using System.Reflection;
using System.Threading;

namespace ProjectManager.Data
{
    public class ProjectManagerContext : DbContext
    {
        public ProjectManagerContext(DbContextOptions<ProjectManagerContext> options) : base(options) { }

        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
            base.OnConfiguring(optionsBuilder);
        }

        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        public override async System.Threading.Tasks.Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            AddTimestamps();
            return await base.SaveChangesAsync(cancellationToken);
        }

        private void AddTimestamps()
        {
            var now = DateTime.Now;
            var updatedEntities = ChangeTracker.Entries()
                .Where(x => x.Entity is Entity && x.State == EntityState.Modified);

            foreach (var entity in updatedEntities)
            {
                ((Entity)entity.Entity).UpdatedAt = now;
            }

            var addedEntities = ChangeTracker.Entries()
                .Where(x => x.Entity is Entity && x.State == EntityState.Added);

            foreach (var entity in addedEntities)
            {
                ((Entity)entity.Entity).CreatedAt = now;
                ((Entity)entity.Entity).UpdatedAt = now;
            }
        }
    }
}
