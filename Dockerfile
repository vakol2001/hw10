FROM mcr.microsoft.com/dotnet/sdk:5.0 AS sdk
WORKDIR /dist

COPY /ProjectManager/ProjectManager.WebAPI/ProjectManager.WebAPI.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet publish ProjectManager/ProjectManager.sln -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /dist
EXPOSE 8082
COPY --from=sdk /dist/out .
ENTRYPOINT [ "dotnet", "ProjectManager.WebAPI.dll" ]
